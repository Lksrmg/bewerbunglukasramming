import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Iterator;
import java.util.NoSuchElementException;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FacebookHTTP {

	public static Logger logger = LoggerFactory.getLogger(FacebookHTTP.class);

	public static void main(String[] args) throws IOException {

		String urlTmp = Credentials.FACEBOOK_PUBLIC_FEED_URL.getValue();
		String url = urlTmp
				.replaceAll(Credentials.access_token_placeholder, Credentials.FACEBOOK_ACCESS_TOKEN_PERM.getValue())
				.replaceAll(Credentials.page_id_placeholder, Credentials.FACEBOOK_PAGE_ID.getValue());

		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");

		int responseCode = con.getResponseCode();
		System.out.println("Hallo Herr Dr. Schenkl! ");
		System.out.println("\nIhre Anfrage gegen die Graph-API von Facebook über die URL: " + url);
		System.out.println("wurde erfolreich mit dem Statuscode: " + responseCode + " beantwortet.");

		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		handleResponse(response.toString());
	}

	/*
	 * Facebook-Seite "Bewerbung-Lukas-Ramming":
	 * https://www.facebook.com/pg/Bewerbung-Lukas-Ramming-377051012875589
	 */
	private static void handleResponse(String string) {
		System.out.println();

		JSONObject response = new JSONObject(string);

		if (response.has("data")) {
			JSONArray data = (JSONArray) response.get("data");

			for (Iterator iterator = data.iterator(); iterator.hasNext();) {
				JSONObject post = (JSONObject) iterator.next();
				if (post.has("message")) {
					System.out.println(post.get("message"));
				}
			}
		} else {
			throw new NoSuchElementException("Die Response enthält keine angeforderten Daten.");
		}

		System.out.println("");

		logger.info(
				"Für die Bewerbung habe ich eine Facebook Seite mit dem Namen 'Bewerbung Lukas Ramming' sowie eine Facebook App angelegt.");
		logger.info(
				"Die Java Anwendung greift alle öffentlichen Posts dieser Seite ab und gibt sie in der Konsole aus.");
		logger.info(
				"Gerne können Sie die Funktionsweise testen, indem Sie einen öffentlichen Post auf die oben genannte Seite posten. Nach anschließendem Ausführen der Anwendung, wird der Post in der Konsole ausgegeben.");

	}

}
