public enum Credentials {

	FACEBOOK_PAGE_ID("377051012875589"),
	FACEBOOK_PUBLIC_FEED_URL("https://graph.facebook.com/v3.2/_PAGE_ID_/feed?access_token=_ACCESS_TOKEN_"),
	FACEBOOK_ACCESS_TOKEN_PERM(
			"EAAEE0bKmtFEBAOpgQstwXGfIS4MIMOsrBoipj4l5e4J7TSzzu0C4CVSz3dv9YiP1oaiZAXTAdZCZB4Pe8YPfc32PLTZCxhlafmlMg9JT93YjjZBor6wPcgJUS73RzFyCTJOSgfM7gNNaCtNnqo1QIh6wx253zuEdZBNVuSw7HHJQZDZD");

	public static final String access_token_placeholder = "_ACCESS_TOKEN_";
	public static final String page_id_placeholder = "_PAGE_ID_";

	private String value;

	Credentials(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}
}